<!--[if !IE]> -->
<script src="<%= request.getContextPath()%>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement)
        document.write("<script src='<%= request.getContextPath()%>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

<script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
  <script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<%= request.getContextPath()%>/js/jquery-ui.custom.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.easypiechart.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.sparkline.index.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.flot.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.flot.pie.min.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->
<script src="<%= request.getContextPath()%>/js/ace-elements.min.js"></script>
<script src="<%= request.getContextPath()%>/js/ace.min.js"></script>