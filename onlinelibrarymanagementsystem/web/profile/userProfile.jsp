<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/start/header_resources.jsp" %>

        <!-- page specific plugin styles -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery-ui.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.gritter.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-editable.min.css" />

    </head>
    <body class="no-skin">

        <%@include file="/start/top_bar.jsp" %>

        <div class="main-container ace-save-state" id="main-container">

            <%@include file="/start/left_manu.jsp" %>

            <div class="main-content">
                <div class="main-content-inner">

                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="Dashboard">Home</a>
                            </li>

                            <li>
                                <a href="UserProfile">Profile</a>
                            </li>
                            <li class="active">User Profile</li>
                        </ul>
                        <!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div>
                        <!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                        <%@include file="/start/settings_box.jsp" %>

                        <div class="page-header">
                            <h1>
                                User Profile
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Details of User
                                </small>
                            </h1>
                        </div>
                        <!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div>
                                    <div id="user-profile-3" class="user-profile row">
                                        <div class="col-sm-offset-1 col-sm-10">
                                            <div class="well well-sm">

                                                <div class="inline middle blue bigger-110"> Your profile is 70% complete </div>

                                                &nbsp; &nbsp; &nbsp;

                                                <div style="width:200px;" data-percent="70%" class="inline middle no-margin progress progress-striped active pos-rel">
                                                    <div class="progress-bar progress-bar-success" style="width:70%"></div>
                                                </div>
                                            </div>
                                            <!-- /.well -->

                                            <div class="space"></div>

                                            <form class="form-horizontal">
                                                <div class="tabbable">
                                                    <ul class="nav nav-tabs padding-16">

                                                        <li class="active">
                                                            <a data-toggle="tab" href="#edit-basic">
                                                                <i class="green ace-icon fa fa-pencil-square-o bigger-125"></i>
                                                                Basic Info
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a data-toggle="tab" href="#edit-settings">
                                                                <i class="purple ace-icon fa fa-cog bigger-125"></i>
                                                                Settings
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a data-toggle="tab" href="#edit-password">
                                                                <i class="blue ace-icon fa fa-key bigger-125"></i>
                                                                Password
                                                            </a>
                                                        </li>
                                                    </ul>

                                                    <div class="tab-content profile-edit-tab-content">

                                                        <s:if test="userInfoList">
                                                            <s:if test="userInfoList">
                                                                <s:iterator value="userInfoList">

                                                                    <div id="edit-basic" class="tab-pane in active">
                                                                        <h4 class="header blue bolder smaller">General</h4>
                                                                        <div class="row">

                                                                            <div class="col-xs-12 col-sm-3 center">
                                                                                <div>
                                                                                    <span class="profile-picture">
                                                                                        <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="User Image " src="UPLOADED_IMAGE/<s:property value="userImage"/>" style="display: block;">
                                                                                    </span>

                                                                                    <div class="space-4"></div>

                                                                                    <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                                                                        <div class="inline position-relative">
                                                                                            <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                                                <i class="ace-icon fa fa-circle light-green"></i>
                                                                                                &nbsp;
                                                                                                <span class="white"><s:property value="fullName"/></span>
                                                                                            </a>

                                                                                            <ul class="align-left dropdown-menu dropdown-caret dropdown-lighter">
                                                                                                <li class="dropdown-header">
                                                                                                    Change Status
                                                                                                </li>
                                                                                                <s:if test="userStatus=='Y'">
                                                                                                    <li>
                                                                                                        <a href="#">
                                                                                                            <i class="ace-icon fa fa-circle green"></i>
                                                                                                            &nbsp;
                                                                                                            <span class="green">Available</span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </s:if>
                                                                                                <s:else>
                                                                                                    <li>
                                                                                                        <a href="#">
                                                                                                            <i class="ace-icon fa fa-circle grey"></i>
                                                                                                            &nbsp;
                                                                                                            <span class="grey">Invisible</span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                </s:else>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="vspace-12-sm"></div>

                                                                            <div class="col-xs-12 col-sm-8">

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-4 control-label no-padding-right" for="userGroup">User Group</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="text" id="userGroup" name="userGroup" value="<s:property value="userGroupInfo.groupName"/>" placeholder="User Group" readonly class="col-xs-12 col-sm-10"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="space-4"></div>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-4 control-label no-padding-right" for="userId">User ID</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="text" id="userId" name="userId" value="<s:property value="userid"/>" placeholder="User ID" readonly class="col-xs-12 col-sm-10"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="space-4"></div>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-4 control-label no-padding-right" for="userName">User Name</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="text" id="userName" name="userName" readonly value="<s:property value="userName"/>" placeholder="User Name" class="col-xs-12 col-sm-10"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="space-4"></div>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-4 control-label no-padding-right" for="firstName">First Name</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="text" id="firstName" name="firstName" value="<s:property value="firstName"/>" placeholder="First Name" class="col-xs-12 col-sm-10"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-4 control-label no-padding-right" for="lastName">Last Name</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="text" id="lastName" name="lastName" value="<s:property value="lastName"/>" placeholder="Last Name" class="col-xs-12 col-sm-10"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <hr />

                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-date">Date of Birth</label>
                                                                            <div class="col-sm-9">
                                                                                <div class="input-medium">
                                                                                    <div class="input-group">
                                                                                        <input class="input-medium date-picker" value="<s:property value="userDob"/>" name="dob" id="form-field-date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"/>
                                                                                        <span class="input-group-addon">
                                                                                            <i class="ace-icon fa fa-calendar"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right">Gender</label>
                                                                            <div class="col-sm-9">
                                                                                <s:if test="gender==1">
                                                                                    <div class="form-group">
                                                                                        <div class="radio">
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="1" class="ace" checked/>
                                                                                                <span class="lbl middle"> Male</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="2" class="ace"/>
                                                                                                <span class="lbl middle"> Female</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="3" class="ace"/>
                                                                                                <span class="lbl middle"> Others</span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </s:if>
                                                                                <s:elseif test="gender==2">
                                                                                    <div class="form-group">
                                                                                        <div class="radio">
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="1" class="ace"/>
                                                                                                <span class="lbl middle"> Male</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="2" class="ace" checked/>
                                                                                                <span class="lbl middle"> Female</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="3" class="ace"/>
                                                                                                <span class="lbl middle"> Others</span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </s:elseif>
                                                                                <s:else>
                                                                                    <div class="form-group">
                                                                                        <div class="radio">
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="1" class="ace"/>
                                                                                                <span class="lbl middle"> Male</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="2" class="ace"/>
                                                                                                <span class="lbl middle"> Female</span>
                                                                                            </label>
                                                                                            <label class="inline">
                                                                                                <input id="gender" name="gender" type="radio" value="3" class="ace" checked/>
                                                                                                <span class="lbl middle"> Others</span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </s:else>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="occupation">Occupation</label>
                                                                            <div class="col-sm-9">
                                                                                <input type="text" id="occupation" name="occupation" value="<s:property value="occupation"/>" placeholder="Occupation" class="col-xs-8 col-sm-6"/>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-comment">Description</label>
                                                                            <div class="col-sm-9">
                                                                                <textarea id="form-field-comment"><s:property value="description"/></textarea>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space"></div>
                                                                        <h4 class="header blue bolder smaller">Contact</h4>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="email">Email</label>
                                                                            <div class="col-sm-9">
                                                                                <span class="input-icon input-icon-right">
                                                                                    <input type="email" id="email" name="email" value="<s:property value="userEmail"/>"/>
                                                                                    <i class="ace-icon fa fa-envelope"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="mobile">Mobile</label>
                                                                            <div class="col-sm-9">
                                                                                <span class="input-icon input-icon-right">
                                                                                    <input class="input-medium" type="text" id="mobile" name="mobile" value="<s:property value="userMobile"/>" />
                                                                                    <i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-comment">Address</label>
                                                                            <div class="col-sm-9">
                                                                                <textarea id="form-field-comment"><s:property value="userAddress"/></textarea>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space"></div>
                                                                        <h4 class="header blue bolder smaller">Social</h4>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-facebook">Facebook</label>

                                                                            <div class="col-sm-9">
                                                                                <span class="input-icon">
                                                                                    <input type="text" value="facebook_alexdoe" id="form-field-facebook" />
                                                                                    <i class="ace-icon fa fa-facebook blue"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-twitter">Twitter</label>
                                                                            <div class="col-sm-9">
                                                                                <span class="input-icon">
                                                                                    <input type="text" value="twitter_alexdoe" id="form-field-twitter" />
                                                                                    <i class="ace-icon fa fa-twitter light-blue"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-gplus">Google+</label>
                                                                            <div class="col-sm-9">
                                                                                <span class="input-icon">
                                                                                    <input type="text" value="google_alexdoe" id="form-field-gplus" />
                                                                                    <i class="ace-icon fa fa-google-plus red"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div id="edit-settings" class="tab-pane">
                                                                        <div class="space-10"></div>
                                                                        <div>
                                                                            <label class="inline">
                                                                                <input type="checkbox" name="form-field-checkbox" class="ace" />
                                                                                <span class="lbl"> Make my profile public</span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="space-8"></div>
                                                                        <div>
                                                                            <label class="inline">
                                                                                <input type="checkbox" name="form-field-checkbox" class="ace" />
                                                                                <span class="lbl"> Email me new updates</span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="space-8"></div>
                                                                        <div>
                                                                            <label>
                                                                                <input type="checkbox" name="form-field-checkbox" class="ace" />
                                                                                <span class="lbl"> Keep a history of my conversations</span>
                                                                            </label>

                                                                            <label>
                                                                                <span class="space-2 block"></span>

                                                                                for
                                                                                <input type="text" class="input-mini" maxlength="3" />
                                                                                days
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                    <div id="edit-password" class="tab-pane">
                                                                        <div class="space-10"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-pass1">New Password</label>

                                                                            <div class="col-sm-9">
                                                                                <input type="password" id="form-field-pass1" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="space-4"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-pass2">Confirm Password</label>
                                                                            <div class="col-sm-9">
                                                                                <input type="password" id="form-field-pass2" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </div>

                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button class="btn btn-info" type="button">
                                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                                            Update
                                                        </button>

                                                        &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- /.span -->
                                    </div>
                                    <!-- /.user-profile -->
                                </div>
                                <!-- PAGE CONTENT ENDS -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.page-content -->
                </div>
            </div>
            <!-- /.main-content -->

            <%@include file="/start/footer.jsp" %>
        </div>
        <!-- /.main-container -->

        <!-- basic scripts -->
        <%@include file="/start/footer_resources.jsp" %>

        <!-- page specific plugin scripts -->

        <!--[if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="<%= request.getContextPath()%>/js/jquery-ui.custom.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.ui.touch-punch.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.gritter.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootbox.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.easypiechart.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.hotkeys.index.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-wysiwyg.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/select2.min.js"></script>
        <script src="/js/spinbox.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-editable.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/ace-editable.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.maskedinput.min.js"></script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function ($) {

                //editables on first profile page
                $.fn.editable.defaults.mode = 'inline';
                $.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
                $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>' +
                        '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';

                //editables 

                //text editable
                $('#username')
                        .editable({
                            type: 'text',
                            name: 'username'
                        });



                //select2 editable
                var countries = [];
                $.each({"CA": "Canada", "IN": "India", "NL": "Netherlands", "TR": "Turkey", "US": "United States"}, function (k, v) {
                    countries.push({id: k, text: v});
                });

                var cities = [];
                cities["CA"] = [];
                $.each(["Toronto", "Ottawa", "Calgary", "Vancouver"], function (k, v) {
                    cities["CA"].push({id: v, text: v});
                });
                cities["IN"] = [];
                $.each(["Delhi", "Mumbai", "Bangalore"], function (k, v) {
                    cities["IN"].push({id: v, text: v});
                });
                cities["NL"] = [];
                $.each(["Amsterdam", "Rotterdam", "The Hague"], function (k, v) {
                    cities["NL"].push({id: v, text: v});
                });
                cities["TR"] = [];
                $.each(["Ankara", "Istanbul", "Izmir"], function (k, v) {
                    cities["TR"].push({id: v, text: v});
                });
                cities["US"] = [];
                $.each(["New York", "Miami", "Los Angeles", "Chicago", "Wysconsin"], function (k, v) {
                    cities["US"].push({id: v, text: v});
                });

                var currentValue = "NL";
                $('#country').editable({
                    type: 'select2',
                    value: 'NL',
                    //onblur:'ignore',
                    source: countries,
                    select2: {
                        'width': 140
                    },
                    success: function (response, newValue) {
                        if (currentValue == newValue)
                            return;
                        currentValue = newValue;

                        var new_source = (!newValue || newValue == "") ? [] : cities[newValue];

                        //the destroy method is causing errors in x-editable v1.4.6+
                        //it worked fine in v1.4.5
                        /**			
                         $('#city').editable('destroy').editable({
                         type: 'select2',
                         source: new_source
                         }).editable('setValue', null);
                         */

                        //so we remove it altogether and create a new element
                        var city = $('#city').removeAttr('id').get(0);
                        $(city).clone().attr('id', 'city').text('Select City').editable({
                            type: 'select2',
                            value: null,
                            //onblur:'ignore',
                            source: new_source,
                            select2: {
                                'width': 140
                            }
                        }).insertAfter(city);//insert it after previous instance
                        $(city).remove();//remove previous instance

                    }
                });

                $('#city').editable({
                    type: 'select2',
                    value: 'Amsterdam',
                    //onblur:'ignore',
                    source: cities[currentValue],
                    select2: {
                        'width': 140
                    }
                });



                //custom date editable
                $('#signup').editable({
                    type: 'adate',
                    date: {
                        //datepicker plugin options
                        format: 'yyyy/mm/dd',
                        viewformat: 'yyyy/mm/dd',
                        weekStart: 1

                                //,nativeUI: true//if true and browser support input[type=date], native browser control will be used
                                //,format: 'yyyy-mm-dd',
                                //viewformat: 'yyyy-mm-dd'
                    }
                })

                $('#age').editable({
                    type: 'spinner',
                    name: 'age',
                    spinner: {
                        min: 16,
                        max: 99,
                        step: 1,
                        on_sides: true
                                //,nativeUI: true//if true and browser support input[type=number], native browser control will be used
                    }
                });


                $('#login').editable({
                    type: 'slider',
                    name: 'login',
                    slider: {
                        min: 1,
                        max: 50,
                        width: 100
                                //,nativeUI: true//if true and browser support input[type=range], native browser control will be used
                    },
                    success: function (response, newValue) {
                        if (parseInt(newValue) == 1)
                            $(this).html(newValue + " hour ago");
                        else
                            $(this).html(newValue + " hours ago");
                    }
                });

                $('#about').editable({
                    mode: 'inline',
                    type: 'wysiwyg',
                    name: 'about',
                    wysiwyg: {
                        //css : {'max-width':'300px'}
                    },
                    success: function (response, newValue) {
                    }
                });



                // *** editable avatar *** //
                try {//ie8 throws some harmless exceptions, so let's catch'em

                    //first let's add a fake appendChild method for Image element for browsers that have a problem with this
                    //because editable plugin calls appendChild, and it causes errors on IE at unpredicted points
                    try {
                        document.createElement('IMG').appendChild(document.createElement('B'));
                    } catch (e) {
                        Image.prototype.appendChild = function (el) {
                        }
                    }

                    var last_gritter
                    $('#avatar').editable({
                        type: 'image',
                        name: 'avatar',
                        value: null,
                        //onblur: 'ignore',  //don't reset or hide editable onblur?!
                        image: {
                            //specify ace file input plugin's options here
                            btn_choose: 'Change Avatar',
                            droppable: true,
                            maxSize: 110000, //~100Kb

                            //and a few extra ones here
                            name: 'avatar', //put the field name here as well, will be used inside the custom plugin
                            on_error: function (error_type) {//on_error function will be called when the selected file has a problem
                                if (last_gritter)
                                    $.gritter.remove(last_gritter);
                                if (error_type == 1) {//file format error
                                    last_gritter = $.gritter.add({
                                        title: 'File is not an image!',
                                        text: 'Please choose a jpg|gif|png image!',
                                        class_name: 'gritter-error gritter-center'
                                    });
                                } else if (error_type == 2) {//file size rror
                                    last_gritter = $.gritter.add({
                                        title: 'File too big!',
                                        text: 'Image size should not exceed 100Kb!',
                                        class_name: 'gritter-error gritter-center'
                                    });
                                }
                                else {//other error
                                }
                            },
                            on_success: function () {
                                $.gritter.removeAll();
                            }
                        },
                        url: function (params) {
                            // ***UPDATE AVATAR HERE*** //
                            //for a working upload example you can replace the contents of this function with 
                            //examples/profile-avatar-update.js

                            var deferred = new $.Deferred

                            var value = $('#avatar').next().find('input[type=hidden]:eq(0)').val();
                            if (!value || value.length == 0) {
                                deferred.resolve();
                                return deferred.promise();
                            }


                            //dummy upload
                            setTimeout(function () {
                                if ("FileReader" in window) {
                                    //for browsers that have a thumbnail of selected image
                                    var thumb = $('#avatar').next().find('img').data('thumb');
                                    if (thumb)
                                        $('#avatar').get(0).src = thumb;
                                }

                                deferred.resolve({'status': 'OK'});

                                if (last_gritter)
                                    $.gritter.remove(last_gritter);
                                last_gritter = $.gritter.add({
                                    title: 'Avatar Updated!',
                                    text: 'Uploading to server can be easily implemented. A working example is included with the template.',
                                    class_name: 'gritter-info gritter-center'
                                });

                            }, parseInt(Math.random() * 800 + 800))

                            return deferred.promise();

                            // ***END OF UPDATE AVATAR HERE*** //
                        },
                        success: function (response, newValue) {
                        }
                    })
                } catch (e) {
                }

                /**
                 //let's display edit mode by default?
                 var blank_image = true;//somehow you determine if image is initially blank or not, or you just want to display file input at first
                 if(blank_image) {
                 $('#avatar').editable('show').on('hidden', function(e, reason) {
                 if(reason == 'onblur') {
                 $('#avatar').editable('show');
                 return;
                 }
                 $('#avatar').off('hidden');
                 })
                 }
                 */

                //another option is using modals
                $('#avatar2').on('click', function () {
                    var modal =
                            '<div class="modal fade">\
                                  <div class="modal-dialog">\
                                   <div class="modal-content">\
                                        <div class="modal-header">\
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>\
                                                <h4 class="blue">Change Avatar</h4>\
                                        </div>\
                                        \
                                        <form class="no-margin">\
                                         <div class="modal-body">\
                                                <div class="space-4"></div>\
                                                <div style="width:75%;margin-left:12%;"><input type="file" name="file-input" /></div>\
                                         </div>\
                                        \
                                         <div class="modal-footer center">\
                                                <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Submit</button>\
                                                <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
                                         </div>\
                                        </form>\
                                  </div>\
                                 </div>\
                                </div>';


                    var modal = $(modal);
                    modal.modal("show").on("hidden", function () {
                        modal.remove();
                    });

                    var working = false;

                    var form = modal.find('form:eq(0)');
                    var file = form.find('input[type=file]').eq(0);
                    file.ace_file_input({
                        style: 'well',
                        btn_choose: 'Click to choose new avatar',
                        btn_change: null,
                        no_icon: 'ace-icon fa fa-picture-o',
                        thumbnail: 'small',
                        before_remove: function () {
                            //don't remove/reset files while being uploaded
                            return !working;
                        },
                        allowExt: ['jpg', 'jpeg', 'png', 'gif'],
                        allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
                    });

                    form.on('submit', function () {
                        if (!file.data('ace_input_files'))
                            return false;

                        file.ace_file_input('disable');
                        form.find('button').attr('disabled', 'disabled');
                        form.find('.modal-body').append("<div class='center'><i class='ace-icon fa fa-spinner fa-spin bigger-150 orange'></i></div>");

                        var deferred = new $.Deferred;
                        working = true;
                        deferred.done(function () {
                            form.find('button').removeAttr('disabled');
                            form.find('input[type=file]').ace_file_input('enable');
                            form.find('.modal-body > :last-child').remove();

                            modal.modal("hide");

                            var thumb = file.next().find('img').data('thumb');
                            if (thumb)
                                $('#avatar2').get(0).src = thumb;

                            working = false;
                        });


                        setTimeout(function () {
                            deferred.resolve();
                        }, parseInt(Math.random() * 800 + 800));

                        return false;
                    });

                });



                //////////////////////////////
                $('#profile-feed-1').ace_scroll({
                    height: '250px',
                    mouseWheelLock: true,
                    alwaysVisible: true
                });

                $('a[ data-original-title]').tooltip();

                $('.easy-pie-chart.percentage').each(function () {
                    var barColor = $(this).data('color') || '#555';
                    var trackColor = '#E2E2E2';
                    var size = parseInt($(this).data('size')) || 72;
                    $(this).easyPieChart({
                        barColor: barColor,
                        trackColor: trackColor,
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: parseInt(size / 10),
                        animate: false,
                        size: size
                    }).css('color', barColor);
                });

                ///////////////////////////////////////////

                //right & left position
                //show the user info on right or left depending on its position
                $('#user-profile-2 .memberdiv').on('mouseenter touchstart', function () {
                    var $this = $(this);
                    var $parent = $this.closest('.tab-pane');

                    var off1 = $parent.offset();
                    var w1 = $parent.width();

                    var off2 = $this.offset();
                    var w2 = $this.width();

                    var place = 'left';
                    if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                        place = 'right';

                    $this.find('.popover').removeClass('right left').addClass(place);
                }).on('click', function (e) {
                    e.preventDefault();
                });


                ///////////////////////////////////////////
                $('#user-profile-3')
                        .find('input[type=file]').ace_file_input({
                    style: 'well',
                    btn_choose: 'Change avatar',
                    btn_change: null,
                    no_icon: 'ace-icon fa fa-picture-o',
                    thumbnail: 'large',
                    droppable: true,
                    allowExt: ['jpg', 'jpeg', 'png', 'gif'],
                    allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
                })
                        .end().find('button[type=reset]').on(ace.click_event, function () {
                    $('#user-profile-3 input[type=file]').ace_file_input('reset_input');
                })
                        .end().find('.date-picker').datepicker().next().on(ace.click_event, function () {
                    $(this).prev().focus();
                })
                $('.input-mask-phone').mask('(999) 999-9999');

                $('#user-profile-3').find('input[type=file]').ace_file_input('show_file_list', [{type: 'image', name: $('#avatar').attr('src')}]);


                ////////////////////
                //change profile
                $('[data-toggle="buttons"] .btn').on('click', function (e) {
                    var target = $(this).find('input[type=radio]');
                    var which = parseInt(target.val());
                    $('.user-profile').parent().addClass('hide');
                    $('#user-profile-' + which).parent().removeClass('hide');
                });



                /////////////////////////////////////
                $(document).one('ajaxloadstart.page', function (e) {
                    //in ajax mode, remove remaining elements before leaving page
                    try {
                        $('.editable').editable('destroy');
                    } catch (e) {
                    }
                    $('[class*=select2]').remove();
                });
            });
        </script>
    </body>
</html>
