<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/start/header_resources.jsp" %>
    </head>
    <body class="no-skin">

        <%@include file="/start/top_bar.jsp" %>

        <div class="main-container ace-save-state" id="main-container">

            <%@include file="/start/left_manu.jsp" %>

            <div class="main-content">
                <div class="main-content-inner">

                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="Dashboard">Home</a>
                            </li>

                            <li>
                                <a href="MessageEvent">Message Pages</a>
                            </li>
                            <li class="active">Messaging</li>
                        </ul>
                        <!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div>
                        <!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                        <%@include file="/start/settings_box.jsp" %>

                        <div class="page-header">
                            <h1>
                                Message
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Messagebox with some messages
                                </small>
                            </h1>
                        </div>
                        <!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="tabbable">
                                            <ul class="inbox-tabs nav nav-tabs padding-16 tab-size-bigger tab-space-1" id="inbox-tabs">
                                                <li class="li-new-mail pull-left active">
                                                    <a style="margin-bottom: 10px; margin-left: -15px;" class="btn-new-mail">
                                                        <span class="btn btn-purple no-border">
                                                            <i class="ace-icon fa fa-envelope bigger-130"></i>
                                                            <span class="bigger-110">Write Your Mail</span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <div class="tab-content no-border no-padding">
                                                <div class="tab-pane in active" id="inbox">
                                                    <div class="message-container">
                                                        <div class="message-navbar clearfix" id="id-message-new-navbar">
                                                            <div class="message-bar">
                                                                <div class="message-toolbar">
                                                                    <button class="btn btn-xs btn-white btn-primary" type="button">
                                                                        <i class="ace-icon fa fa-floppy-o bigger-125"></i>
                                                                        <span class="bigger-110">Save Draft</span>
                                                                    </button>

                                                                    <button class="btn btn-xs btn-white btn-primary" type="button">
                                                                        <i class="ace-icon fa fa-times bigger-125 orange2"></i>
                                                                        <span class="bigger-110">Discard</span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="message-list-container">
                                                            <form class="form-horizontal message-form col-xs-12" id="id-message-form" method="post" action="SendMessage" enctype="multipart/form-data">
                                                                <div>
                                                                    <div class="form-group">
                                                                        <label for="form-field-recipient" class="col-sm-3 control-label no-padding-right">Recipient:</label>
                                                                        <div class="col-sm-9">
                                                                            <span class="input-icon">
                                                                                <input style="width: 300%" type="email" placeholder="Recipient(s)" value="" data-value="" id="form-field-recipient" required="" name="recipient">
                                                                                <i class="ace-icon fa fa-user"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="hr hr-18 dotted"></div>
                                                                    <div class="form-group">
                                                                        <label for="form-field-subject" class="col-sm-3 control-label no-padding-right">Subject:</label>
                                                                        <div class="col-sm-6 col-xs-12">
                                                                            <div class="input-icon block col-xs-12 no-padding">
                                                                                <input type="text" placeholder="Subject" id="form-field-subject" name="subject" class="col-xs-12" maxlength="100" required="">
                                                                                <i class="ace-icon fa fa-comment-o"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr hr-18 dotted"></div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">
                                                                            <span class="inline space-24 hidden-480"></span>
                                                                            Message:
                                                                        </label>
                                                                        <div class="col-sm-9">
                                                                            <div class="wysiwyg-toolbar btn-toolbar center wysiwyg-style1"> <div class="btn-group">  <a title="" data-edit="bold" class="btn btn-sm btn-default" data-original-title="Bold (Ctrl/Cmd+B)"><i class=" ace-icon fa fa-bold"></i></a>  <a title="" data-edit="italic" class="btn btn-sm btn-default" data-original-title="Italic (Ctrl/Cmd+I)"><i class=" ace-icon fa fa-italic"></i></a>  <a title="" data-edit="strikethrough" class="btn btn-sm btn-default" data-original-title="Strikethrough"><i class=" ace-icon fa fa-strikethrough"></i></a>  <a title="" data-edit="underline" class="btn btn-sm btn-default" data-original-title="Underline"><i class=" ace-icon fa fa-underline"></i></a>  </div> <div class="btn-group">  <a title="" data-edit="justifyleft" class="btn btn-sm btn-default active" data-original-title="Align Left (Ctrl/Cmd+L)"><i class=" ace-icon fa fa-align-left"></i></a>  <a title="" data-edit="justifycenter" class="btn btn-sm btn-default" data-original-title="Center (Ctrl/Cmd+E)"><i class=" ace-icon fa fa-align-center"></i></a>  <a title="" data-edit="justifyright" class="btn btn-sm btn-default" data-original-title="Align Right (Ctrl/Cmd+R)"><i class=" ace-icon fa fa-align-right"></i></a>  </div> <div class="btn-group">  <div class="btn-group"> <a title="" data-toggle="dropdown" class="btn btn-sm btn-default dropdown-toggle" data-original-title="Hyperlink"><i class=" ace-icon fa fa-link"></i></a>  <div class="dropdown-menu dropdown-caret dropdown-menu-right">							 <div class="input-group">								<input type="text" data-edit="createLink" placeholder="URL" class="form-control">								<span class="input-group-btn">									<button type="button" class="btn btn-sm btn-primary">Add</button>								</span>							 </div>						</div> </div> <a title="" data-edit="unlink" class="btn btn-sm btn-default" data-original-title="Remove Hyperlink"><i class=" ace-icon fa fa-chain-broken"></i></a>  </div> <div class="btn-group">  <a title="" data-edit="undo" class="btn btn-sm btn-default" data-original-title="Undo (Ctrl/Cmd+Z)"><i class=" ace-icon fa fa-undo"></i></a>  <a title="" data-edit="redo" class="btn btn-sm btn-default" data-original-title="Redo (Ctrl/Cmd+Y)"><i class=" ace-icon fa fa-repeat"></i></a>  </div>  </div> <div contenteditable="true" id="messageDiv" class="wysiwyg-editor"></div>
                                                                            <textarea hidden="" name="messageBody" id="messageBody"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr hr-18 dotted"></div>

                                                                    <div class="form-group no-margin-bottom">
                                                                        <label class="col-sm-3 control-label no-padding-right">Attachments:</label>
                                                                        <div class="col-sm-9">
                                                                            <div id="form-attachments">
                                                                                <div class="form-group file-input-container"><div class="col-sm-7"><label class="ace-file-input width-90 inline"><input type="file" name="attachment" id="attachment"><span data-title="Choose" class="ace-file-container"><span data-title="No File ..." class="ace-file-name"><i class=" ace-icon fa fa-upload"></i></span></span><a href="#" class="remove"><i class=" ace-icon fa fa-times"></i></a></label></div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="align-right">
                                                                        <button class="btn btn-sm btn-purple" type="submit" id="">
                                                                            Send
                                                                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="space"></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.tabbable -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                                <!-- PAGE CONTENT ENDS -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.page-content -->
                </div>
            </div>
            <!-- /.main-content -->

            <%@include file="/start/footer.jsp" %>
        </div>
        <!-- /.main-container -->

        <!-- basic scripts -->
        <%@include file="/start/footer_resources.jsp" %>

        <!-- page specific plugin scripts -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-tag.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.hotkeys.index.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-wysiwyg.min.js"></script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function ($) {

                //handling tabs and loading/displaying relevant messages and forms
                //not needed if using the alternative view, as described in docs
                $('#inbox-tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    var currentTab = $(e.target).data('target');
                    if (currentTab == 'write') {
                        Inbox.show_form();
                    } else if (currentTab == 'inbox') {
                        Inbox.show_list();
                    }
                })



                //basic initializations
                $('.message-list .message-item input[type=checkbox]').removeAttr('checked');
                $('.message-list').on('click', '.message-item input[type=checkbox]', function () {
                    $(this).closest('.message-item').toggleClass('selected');
                    if (this.checked)
                        Inbox.display_bar(1);//display action toolbar when a message is selected
                    else {
                        Inbox.display_bar($('.message-list input[type=checkbox]:checked').length);
                        //determine number of selected messages and display/hide action toolbar accordingly
                    }
                });


                //check/uncheck all messages
                $('#id-toggle-all').removeAttr('checked').on('click', function () {
                    if (this.checked) {
                        Inbox.select_all();
                    } else
                        Inbox.select_none();
                });

                //select all
                $('#id-select-message-all').on('click', function (e) {
                    e.preventDefault();
                    Inbox.select_all();
                });

                //select none
                $('#id-select-message-none').on('click', function (e) {
                    e.preventDefault();
                    Inbox.select_none();
                });

                //select read
                $('#id-select-message-read').on('click', function (e) {
                    e.preventDefault();
                    Inbox.select_read();
                });

                //select unread
                $('#id-select-message-unread').on('click', function (e) {
                    e.preventDefault();
                    Inbox.select_unread();
                });

                /////////

                //display first message in a new area
                $('.message-list .message-item:eq(0) .text').on('click', function () {
                    //show the loading icon
                    $('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');

                    $('.message-inline-open').removeClass('message-inline-open').find('.message-content').remove();

                    var message_list = $(this).closest('.message-list');

                    $('#inbox-tabs a[href="#inbox"]').parent().removeClass('active');
                    //some waiting
                    setTimeout(function () {

                        //hide everything that is after .message-list (which is either .message-content or .message-form)
                        message_list.next().addClass('hide');
                        $('.message-container').find('.message-loading-overlay').remove();

                        //close and remove the inline opened message if any!

                        //hide all navbars
                        $('.message-navbar').addClass('hide');
                        //now show the navbar for single message item
                        $('#id-message-item-navbar').removeClass('hide');

                        //hide all footers
                        $('.message-footer').addClass('hide');
                        //now show the alternative footer
                        $('.message-footer-style2').removeClass('hide');


                        //move .message-content next to .message-list and hide .message-list
                        $('.message-content').removeClass('hide').insertAfter(message_list.addClass('hide'));

                        //add scrollbars to .message-body
                        $('.message-content .message-body').ace_scroll({
                            size: 150,
                            mouseWheelLock: true,
                            styleClass: 'scroll-visible'
                        });

                    }, 500 + parseInt(Math.random() * 500));
                });


                //display second message right inside the message list
                $('.message-list .message-item:eq(1) .text').on('click', function () {
                    var message = $(this).closest('.message-item');

                    //if message is open, then close it
                    if (message.hasClass('message-inline-open')) {
                        message.removeClass('message-inline-open').find('.message-content').remove();
                        return;
                    }

                    $('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');
                    setTimeout(function () {
                        $('.message-container').find('.message-loading-overlay').remove();
                        message
                                .addClass('message-inline-open')
                                .append('<div class="message-content" />')
                        var content = message.find('.message-content:last').html($('#id-message-content').html());

                        //remove scrollbar elements
                        content.find('.scroll-track').remove();
                        content.find('.scroll-content').children().unwrap();

                        content.find('.message-body').ace_scroll({
                            size: 150,
                            mouseWheelLock: true,
                            styleClass: 'scroll-visible'
                        });

                    }, 500 + parseInt(Math.random() * 500));

                });



                //back to message list
                $('.btn-back-message-list').on('click', function (e) {

                    e.preventDefault();
                    $('#inbox-tabs a[href="#inbox"]').tab('show');
                });



                //hide message list and display new message form
                /**
                 $('.btn-new-mail').on('click', function(e){
                 e.preventDefault();
                 Inbox.show_form();
                 });
                 */




                var Inbox = {
                    //displays a toolbar according to the number of selected messages
                    display_bar: function (count) {
                        if (count == 0) {
                            $('#id-toggle-all').removeAttr('checked');
                            $('#id-message-list-navbar .message-toolbar').addClass('hide');
                            $('#id-message-list-navbar .message-infobar').removeClass('hide');
                        } else {
                            $('#id-message-list-navbar .message-infobar').addClass('hide');
                            $('#id-message-list-navbar .message-toolbar').removeClass('hide');
                        }
                    }
                    ,
                    select_all: function () {
                        var count = 0;
                        $('.message-item input[type=checkbox]').each(function () {
                            this.checked = true;
                            $(this).closest('.message-item').addClass('selected');
                            count++;
                        });

                        $('#id-toggle-all').get(0).checked = true;

                        Inbox.display_bar(count);
                    }
                    ,
                    select_none: function () {
                        $('.message-item input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');
                        $('#id-toggle-all').get(0).checked = false;

                        Inbox.display_bar(0);
                    }
                    ,
                    select_read: function () {
                        $('.message-unread input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');

                        var count = 0;
                        $('.message-item:not(.message-unread) input[type=checkbox]').each(function () {
                            this.checked = true;
                            $(this).closest('.message-item').addClass('selected');
                            count++;
                        });
                        Inbox.display_bar(count);
                    }
                    ,
                    select_unread: function () {
                        $('.message-item:not(.message-unread) input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');

                        var count = 0;
                        $('.message-unread input[type=checkbox]').each(function () {
                            this.checked = true;
                            $(this).closest('.message-item').addClass('selected');
                            count++;
                        });

                        Inbox.display_bar(count);
                    }
                }

                //show message list (back from writing mail or reading a message)
                Inbox.show_list = function () {
                    $('.message-navbar').addClass('hide');
                    $('#id-message-list-navbar').removeClass('hide');

                    $('.message-footer').addClass('hide');
                    $('.message-footer:not(.message-footer-style2)').removeClass('hide');

                    $('.message-list').removeClass('hide').next().addClass('hide');
                    //hide the message item / new message window and go back to list
                }

                //show write mail form
                Inbox.show_form = function () {
                    if ($('.message-form').is(':visible'))
                        return;
                    if (!form_initialized) {
                        initialize_form();
                    }


                    var message = $('.message-list');
                    $('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');

                    setTimeout(function () {
                        message.next().addClass('hide');

                        $('.message-container').find('.message-loading-overlay').remove();

                        $('.message-list').addClass('hide');
                        $('.message-footer').addClass('hide');
                        $('.message-form').removeClass('hide').insertAfter('.message-list');

                        $('.message-navbar').addClass('hide');
                        $('#id-message-new-navbar').removeClass('hide');


                        //reset form??
                        $('.message-form .wysiwyg-editor').empty();

                        $('.message-form .ace-file-input').closest('.file-input-container:not(:first-child)').remove();
                        $('.message-form input[type=file]').ace_file_input('reset_input');

                        $('.message-form').get(0).reset();

                    }, 300 + parseInt(Math.random() * 300));
                }




                var form_initialized = false;
                function initialize_form() {
                    if (form_initialized)
                        return;
                    form_initialized = true;

                    //intialize wysiwyg editor
                    $('.message-form .wysiwyg-editor').ace_wysiwyg({
                        toolbar:
                                [
                                    'bold',
                                    'italic',
                                    'strikethrough',
                                    'underline',
                                    null,
                                    'justifyleft',
                                    'justifycenter',
                                    'justifyright',
                                    null,
                                    'createLink',
                                    'unlink',
                                    null,
                                    'undo',
                                    'redo'
                                ]
                    }).prev().addClass('wysiwyg-style1');

                    //file input
                    $('.message-form input[type=file]').ace_file_input()
                            .closest('.ace-file-input')
                            .addClass('width-90 inline')
                            .wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>');

                    //Add Attachment
                    //the button to add a new file input
                    $('#id-add-attachment')
                            .on('click', function () {
                                var file = $('<input type="file" name="attachment[]" />').appendTo('#form-attachments');
                                file.ace_file_input();

                                file.closest('.ace-file-input')
                                        .addClass('width-90 inline')
                                        .wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>')
                                        .parent().append('<div class="action-buttons pull-right col-xs-1">\
                                                <a href="#" data-action="delete" class="middle">\
                                                        <i class="ace-icon fa fa-trash-o red bigger-130 middle"></i>\
                                                </a>\
                                        </div>')
                                        .find('a[data-action=delete]').on('click', function (e) {
                                    //the button that removes the newly inserted file input
                                    e.preventDefault();
                                    $(this).closest('.file-input-container').hide(300, function () {
                                        $(this).remove();
                                    });
                                });
                            });
                }
                //initialize_form

                //turn the recipient field into a tag input field!
                /**	
                 var tag_input = $('#form-field-recipient');
                 try { 
                 tag_input.tag({placeholder:tag_input.attr('placeholder')});
                 } catch(e) {}
                 
                 
                 //and add form reset functionality
                 $('#id-message-form').on('reset', function(){
                 $('.message-form .message-body').empty();
                 
                 $('.message-form .ace-file-input:not(:first-child)').remove();
                 $('.message-form input[type=file]').ace_file_input('reset_input_ui');
                 
                 var val = tag_input.data('value');
                 tag_input.parent().find('.tag').remove();
                 $(val.split(',')).each(function(k,v){
                 tag_input.before('<span class="tag">'+v+'<button class="close" type="button">&times;</button></span>');
                 });
                 });
                 */

                $("form").submit(function () {
                    $("#messageBody").val($("#messageDiv").text());
                });
            });
        </script>
    </body>
</html>
