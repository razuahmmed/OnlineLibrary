<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    // Menu Start 
    // Dashboard
    boolean DASHBOARD = (request.getSession().getAttribute("1") != null && request.getSession().getAttribute("1").toString().equalsIgnoreCase("T")) ? true : false;
    // Admin   
    boolean ADMIN = (request.getSession().getAttribute("2") != null && request.getSession().getAttribute("2").toString().equalsIgnoreCase("T")) ? true : false;
    // Admin Child
    boolean CRADMINUSER = (request.getSession().getAttribute("3") != null && request.getSession().getAttribute("3").toString().equalsIgnoreCase("T")) ? true : false;
    boolean ALLADMINUSER = (request.getSession().getAttribute("4") != null && request.getSession().getAttribute("4").toString().equalsIgnoreCase("T")) ? true : false;
    boolean BOOKCATEGORY = (request.getSession().getAttribute("5") != null && request.getSession().getAttribute("5").toString().equalsIgnoreCase("T")) ? true : false;
    boolean ALLBOOKS = (request.getSession().getAttribute("6") != null && request.getSession().getAttribute("6").toString().equalsIgnoreCase("T")) ? true : false;
    boolean ALLLIB = (request.getSession().getAttribute("7") != null && request.getSession().getAttribute("7").toString().equalsIgnoreCase("T")) ? true : false;
    boolean ALLSTU = (request.getSession().getAttribute("8") != null && request.getSession().getAttribute("8").toString().equalsIgnoreCase("T")) ? true : false;
    boolean SETTINGS = (request.getSession().getAttribute("9") != null && request.getSession().getAttribute("9").toString().equalsIgnoreCase("T")) ? true : false;
    // Book
    boolean BOOK = (request.getSession().getAttribute("10") != null && request.getSession().getAttribute("10").toString().equalsIgnoreCase("T")) ? true : false;
    // Book Child
    boolean CRBOOKCATEGORY = (request.getSession().getAttribute("11") != null && request.getSession().getAttribute("11").toString().equalsIgnoreCase("T")) ? true : false;
    boolean CATHIS = (request.getSession().getAttribute("12") != null && request.getSession().getAttribute("12").toString().equalsIgnoreCase("T")) ? true : false;
    boolean BOOKUP = (request.getSession().getAttribute("13") != null && request.getSession().getAttribute("13").toString().equalsIgnoreCase("T")) ? true : false;
    boolean BOOKHIS = (request.getSession().getAttribute("14") != null && request.getSession().getAttribute("14").toString().equalsIgnoreCase("T")) ? true : false;
    // Librarian
    boolean LIBRARIAN = (request.getSession().getAttribute("15") != null && request.getSession().getAttribute("15").toString().equalsIgnoreCase("T")) ? true : false;
    // Librarian Child
    boolean CRLIBRARIAN = (request.getSession().getAttribute("16") != null && request.getSession().getAttribute("16").toString().equalsIgnoreCase("T")) ? true : false;
    boolean LIBRARIANHIS = (request.getSession().getAttribute("17") != null && request.getSession().getAttribute("17").toString().equalsIgnoreCase("T")) ? true : false;
    // Student
    boolean STUDENT = (request.getSession().getAttribute("18") != null && request.getSession().getAttribute("18").toString().equalsIgnoreCase("T")) ? true : false;
    // Student Child
    boolean CRSTUDENT = (request.getSession().getAttribute("19") != null && request.getSession().getAttribute("19").toString().equalsIgnoreCase("T")) ? true : false;
    boolean STUDENTHIS = (request.getSession().getAttribute("20") != null && request.getSession().getAttribute("20").toString().equalsIgnoreCase("T")) ? true : false;
    // Calendar
    boolean CALENDAR = (request.getSession().getAttribute("21") != null && request.getSession().getAttribute("21").toString().equalsIgnoreCase("T")) ? true : false;
    // Gallery
    boolean GALLERY = (request.getSession().getAttribute("22") != null && request.getSession().getAttribute("22").toString().equalsIgnoreCase("T")) ? true : false;
%>
<script type="text/javascript">
    try {
        ace.settings.loadState('main-container');
    } catch (e) {
    }
</script>
<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar');
        } catch (e) {
        }
    </script>
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">

        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>
            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>
            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>
            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>

    </div>
    <!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">

        <% if (DASHBOARD) {%>
        <li class="<s:if test='menuParent=="Dashboard"'>active</s:if>">
                <a href="Dashboard">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text">Dashboard</span>
                </a>
                <b class="arrow"></b>
            </li>
        <% }%>

        <% if (ADMIN) {%>
        <li class="<s:if test='menuParent=="Admin"'>active open</s:if>">

                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text">
                        Admin
                    </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                <% if (CRADMINUSER) {%>
                <li class="<s:if test='menuChlid=="CreateAdminUser"'>active</s:if>">
                        <a href="CreateAdminUser">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Create Admin User
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (ALLADMINUSER) {%>
                <li class="<s:if test='menuChlid=="AllAdminUsers"'>active</s:if>">
                        <a href="AllAdminUsers">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Admin Users
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (ALLLIB) {%>
                <li class="<s:if test='menuChlid=="AllLibrarians"'>active</s:if>">
                        <a href="AllLibrarians">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Librarians
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (ALLSTU) {%>
                <li class="<s:if test='menuChlid=="AllStudents"'>active</s:if>">
                        <a href="AllStudents">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Students
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (BOOKCATEGORY) {%>
                <li class="<s:if test='menuChlid=="AllBookCategories"'>active</s:if>">
                        <a href="AllBookCategoriesByAdmin">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Book Categories
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (ALLBOOKS) {%>
                <li class="<s:if test='menuChlid=="AllBooks"'>active</s:if>">
                        <a href="AllBooks">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Books
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (ALLBOOKS) {%>
                <li class="<s:if test='menuChlid=="AllBookRentHistory"'>active</s:if>">
                        <a href="AllBookRentHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            All Book Rent History
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (SETTINGS) {%>
                <li class="<s:if test='menuChlid=="Settings"'>active</s:if>">
                        <a href="Settings">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Settings
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>
            </ul>
        </li>
        <% }%>

        <% if (BOOK) {%>
        <li class="<s:if test='menuParent=="Book"'>active open</s:if>">

                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text">Book</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                    <li class="<s:if test='menuChlid=="BookRent"'>active</s:if>">
                        <a href="BookRent">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book Rent
                        </a>
                        <b class="arrow"></b>
                    </li>

                    <li class="<s:if test='menuChlid=="BookRentHistory"'>active</s:if>">
                        <a href="BookRentHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book Rent History
                        </a>
                        <b class="arrow"></b>
                    </li>

                <% if (CRBOOKCATEGORY) {%>
                <li class="<s:if test='menuChlid=="BookCategory"'>active</s:if>">
                        <a href="BookCategory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book Category
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (CATHIS) {%>
                <li class="<s:if test='menuChlid=="CategoryHistory"'>active</s:if>">
                        <a href="CategoryHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book Category History
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (BOOKUP) {%>
                <li class="<s:if test='menuChlid=="UploadBook"'>active</s:if>">
                        <a href="UploadBook">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book Upload
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (BOOKHIS) {%>
                <li class="<s:if test='menuChlid=="BookHistory"'>active</s:if>">
                        <a href="BookHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Book History
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>
            </ul>
        </li>
        <% }%>

        <% if (LIBRARIAN) {%>
        <li class="<s:if test='menuParent=="Librarian"'>active open</s:if>">

                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text">Librarian</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                <% if (CRLIBRARIAN) {%>
                <li class="<s:if test='menuChlid=="CreateLibrarian"'>active</s:if>">
                        <a href="CreateLibrarian">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Create Librarian
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (LIBRARIANHIS) {%>
                <li class="<s:if test='menuChlid=="LibrarianHistory"'>active</s:if>">
                        <a href="LibrarianHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Librarian History
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>
            </ul>
        </li>
        <% }%>

        <% if (STUDENT) {%>
        <li class="<s:if test='menuParent=="Student"'>active open</s:if>">

                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text">Student</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                <% if (CRSTUDENT) {%>
                <li class="<s:if test='menuChlid=="CreateStudent"'>active</s:if>">
                        <a href="CreateStudent">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Create Student
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>

                <% if (STUDENTHIS) {%>
                <li class="<s:if test='menuChlid=="StudentHistory"'>active</s:if>">
                        <a href="StudentHistory">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Student History
                        </a>
                        <b class="arrow"></b>
                    </li>
                <% }%>
            </ul>
        </li>
        <% }%>

        <% if (CALENDAR) {%>
        <li class="<s:if test='menuParent=="CalendarEv"'>active</s:if>">
                <a href="CalendarEvent">
                    <i class="menu-icon fa fa-calendar"></i>
                    <span class="menu-text">
                        Calendar
                        <span class="badge badge-transparent tooltip-error" title="2 Important Events">
                            <i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
                        </span>
                    </span>
                </a>
                <b class="arrow"></b>
            </li>
        <% }%>

        <% if (GALLERY) {%>
        <li class="<s:if test='menuParent=="Gallery"'>active</s:if>">
                <a href="GalleryEvent">
                    <i class="menu-icon fa fa-picture-o"></i>
                    <span class="menu-text">Gallery</span>
                </a>
                <b class="arrow"></b>
            </li>
        <% }%>

        <li class="<s:if test='menuParent=="Message"'>active</s:if>">
            <a href="MessageEvent">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span class="menu-text">Messaging</span>
            </a>
            <b class="arrow"></b>
        </li>
    </ul>
    <!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>