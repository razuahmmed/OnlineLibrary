<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="bookCategoryInfos != null ">
    <s:iterator value="bookCategoryInfos">
        <form action="UpdateBookCategory" method="post" class="form-horizontal" role="form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header no-padding">
                        <div class="table-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <span class="white">&times;</span>
                            </button>
                            Results for Book Category Details
                        </div>
                    </div>

                    <div class="modal-body no-padding">
                        <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                            <thead>
                                <tr>
                                    <th>Book Category Id</th>
                                    <th><span class="col-xs-10 col-sm-10"><s:property value="bookCategoryId"/></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Book Category Name</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="bookCategoryName"/>" type="text" id="bookCategoryName" name="bookCategoryName" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Category Name" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Category Description</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <textarea id="categoryDescription" name="categoryDescription" placeholder="Category Description" maxlength="150" class="col-xs-10 col-sm-10"><s:property value="categoryDescription"/></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Created By</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="userInfo.fullName"/></span></td>
                                </tr>
                                <tr>
                                    <td>Created Date</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="insertDate"/></span></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <select id="status" name="status">
                                                <s:if test="status=='Y'">
                                                    <option selected value="Y">Active</option>
                                                    <option value="N">Inactive</option>
                                                </s:if>
                                                <s:else>
                                                    <option value="Y">Active</option>
                                                    <option selected value="N">Inactive</option>
                                                </s:else>
                                            </select>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">

                                        <input type="hidden" id="bookCategoryId" name="bookCategoryId" value="<s:property value="bookCategoryId"/>"/>

                                        <button type="submit" class="btn btn-info">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Update
                                        </button>

                                        <button type="reset" class="btn">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer no-margin-top">
                        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </s:iterator>
</s:if>