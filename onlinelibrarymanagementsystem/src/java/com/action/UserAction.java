package com.action;

import com.common.AllList;
import com.common.DBConnection;
import com.model.UserDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.UserInfo;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;

public class UserAction extends ActionSupport {

    private String userid;
    private String firstName;
    private String lastName;
    private String userName;
    private String dob;
    private Integer gender;
    private String password;
    private String mobile;
    private String email;
    private String occupation;
    private String description;
    private String address;
    private Integer groupid;
    private String userType;
    private Character userStatus;

    private File userImage;
    private String userImageFileName;
    private String destPath;

    private String menuParent;
    private String menuChlid;
    private String messageString;
    private String messageColor;

    private Connection connection = null;
    private AllList allList = null;
    private UserDao userDao = null;

    private List<UserInfo> userInfoList = null;

    public String addNewUser() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (userDao == null) {
            userDao = new UserDao();
        }

        if (connection != null) {

            if (checkBookProperty(userName) == 0) {

                if (userIDGenerator() != null) {
                    userid = userIDGenerator();
                } else {
                    userid = "100001";
                }

                try {

                    String ext = FilenameUtils.getExtension(userImageFileName);
                    String imgName = userid + "." + ext;

                    destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/UPLOADED_IMAGE");

                    File fileToCreate = new File(destPath, imgName);
                    FileUtils.copyFile(userImage, fileToCreate);

                    int checked = userDao.newUser(connection, userid, firstName, lastName, userName, dob, gender, password,
                            mobile, email, occupation, description, imgName, address, groupid, userId);

                    if (checked > 0) {
                        setMessageColor("success");
                        setMessageString(userType + " created successfully");
                    } else {
                        setMessageColor("danger");
                        setMessageString(userType + " create failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                setMessageColor("danger");
                setMessageString("User Name already exist, Please use different User Name");
            }
        }

        return SUCCESS;
    }

    public String updateUser() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (userDao == null) {
            userDao = new UserDao();
        }

        if (connection != null) {

            if (checkBookProperty(userName) == 0) {

                System.out.println("userid " + userid);
                System.out.println("userStatus " + userStatus);
                System.out.println("gender " + gender);

                try {

//                    String ext = FilenameUtils.getExtension(userImageFileName);
//                    String imgName = userid + "." + ext;
//
//                    destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/UPLOADED_IMAGE");
//
//                    File fileToCreate = new File(destPath, imgName);
//                    FileUtils.copyFile(userImage, fileToCreate);
//
                    int checked = 0;
//                    = userDao.newUser(connection, userid, firstName, lastName, userName, dob, gender, password,
//                            mobile, email, occupation, description, imgName, address, groupid, userId);

                    if (checked > 0) {
                        setMessageColor("success");
                        setMessageString(userType + " updated successfully");
                    } else {
                        setMessageColor("danger");
                        setMessageString(userType + " update failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                setMessageColor("danger");
                setMessageString("User Name already exist, Please use different User Name");
            }
        }

        return SUCCESS;
    }

    private String userIDGenerator() {

        String applicantID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.U_ID)+1),6,'0') "
                        + " AS US_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(USER_ID, 1,6),UNSIGNED INTEGER) "
                        + " AS U_ID FROM user_info)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    applicantID = rs.getString("US_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return applicantID;
    }

    private int checkBookProperty(String property) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT "
                        + " COUNT(USER_NAME) "
                        + " TOTAL "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " USER_NAME = '" + property + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    public String goCreateAdminUserPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        setMenuParent("Admin");
        setMenuChlid("CreateAdminUser");

        return SUCCESS;
    }

    public String goCreateLibrarianPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        setMenuParent("Librarian");
        setMenuChlid("CreateLibrarian");

        return SUCCESS;
    }

    public String goCreateStudentPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        setMenuParent("Student");
        setMenuChlid("CreateStudent");

        return SUCCESS;
    }

    public String allAdminUsersByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "all", "1", "", "", ""));
        }

        setMenuParent("Admin");
        setMenuChlid("AllAdminUsers");

        return SUCCESS;
    }

    public String allLibrariansByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "all", "2", "", "", ""));
        }

        setMenuParent("Admin");
        setMenuChlid("AllLibrarians");

        return SUCCESS;
    }

    public String allStudentsByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "all", "3", "", "", ""));
        }

        setMenuParent("Admin");
        setMenuChlid("AllStudents");

        return SUCCESS;
    }

    public String goLibrarianHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "", "2", userId, "", ""));
        }

        setMenuParent("Librarian");
        setMenuChlid("LibrarianHistory");

        return SUCCESS;
    }

    public String goStudentHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "", "3", userId, "", ""));
        }

        setMenuParent("Student");
        setMenuChlid("StudentHistory");

        return SUCCESS;
    }

    public String goUserProfilePage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "", "", "", userId, ""));
        }

        return SUCCESS;
    }

    public String viewUserDetails() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "", "", "", userid, ""));
        }

        return SUCCESS;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation the occupation to set
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the groupid
     */
    public Integer getGroupid() {
        return groupid;
    }

    /**
     * @param groupid the groupid to set
     */
    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    /**
     * @return the userInfoList
     */
    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    /**
     * @param userInfoList the userInfoList to set
     */
    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    /**
     * @return the menuParent
     */
    public String getMenuParent() {
        return menuParent;
    }

    /**
     * @param menuParent the menuParent to set
     */
    public void setMenuParent(String menuParent) {
        this.menuParent = menuParent;
    }

    /**
     * @return the menuChlid
     */
    public String getMenuChlid() {
        return menuChlid;
    }

    /**
     * @param menuChlid the menuChlid to set
     */
    public void setMenuChlid(String menuChlid) {
        this.menuChlid = menuChlid;
    }

    /**
     * @return the userImage
     */
    public File getUserImage() {
        return userImage;
    }

    /**
     * @param userImage the userImage to set
     */
    public void setUserImage(File userImage) {
        this.userImage = userImage;
    }

    /**
     * @return the userImageFileName
     */
    public String getUserImageFileName() {
        return userImageFileName;
    }

    /**
     * @param userImageFileName the userImageFileName to set
     */
    public void setUserImageFileName(String userImageFileName) {
        this.userImageFileName = userImageFileName;
    }

    /**
     * @return the destPath
     */
    public String getDestPath() {
        return destPath;
    }

    /**
     * @param destPath the destPath to set
     */
    public void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    /**
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return the userStatus
     */
    public Character getUserStatus() {
        return userStatus;
    }

    /**
     * @param userStatus the userStatus to set
     */
    public void setUserStatus(Character userStatus) {
        this.userStatus = userStatus;
    }
}
