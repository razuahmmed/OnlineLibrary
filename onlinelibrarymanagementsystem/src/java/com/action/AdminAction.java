package com.action;

import com.common.AllList;
import com.common.DBConnection;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.MenuMaster;
import com.persistance.UserGroup;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

public class AdminAction extends ActionSupport {

    private String userGroupId;
    private String formMenuIdStatus;

    private String menuParent;
    private String menuChlid;
    private String messageString;
    private String messageColor;

    private Connection connection = null;
    private AllList allList = null;

    private List<MenuMaster> menuMasterInfoList = null;
    private List<UserLevelMenu> userLevelMenuInfoList = null;
    private List<UserGroup> userGroupInfoList = null;

    public String menuBySelectedGroup() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

            List<UserLevelMenu> checked = allList.getMenuByGroup(connection, userGroupId);
            if (checked.size() != 0) {
                setUserLevelMenuInfoList(allList.getMenuByGroup(connection, userGroupId));
            } else {
                setMenuMasterInfoList(allList.getMenuParentInfo(connection));
            }
        }

        return SUCCESS;
    }

    public String saveMenuPermission() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        String groupName = (String) session.get("GROUPNAME");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        PreparedStatement ps = null;

        String[] firstArray = null;
        String[] secondArray = null;
        String menuID = null;
        String menuStatus = null;

        String insertStatement = "";
        String updateStatement = "";
        int status = 0;

        if (connection != null) {
            if ((userGroupId != null) && (formMenuIdStatus != null)) {
                if ((!userGroupId.isEmpty()) && (!formMenuIdStatus.toString().isEmpty())) {
                    firstArray = formMenuIdStatus.split("/mstatus/");
                    if (firstArray != null) {
                        for (int i = 0; i < firstArray.length; i++) {
                            if ((firstArray[i] != null) && (!firstArray[i].isEmpty())) {
                                secondArray = firstArray[i].split("/mid/");
                                if (secondArray != null) {
                                    menuID = secondArray[0];
                                    menuStatus = secondArray[1];
                                    if ((menuID != null) && (menuStatus != null)) {
                                        if ((!menuID.isEmpty()) && (!menuStatus.isEmpty())) {
                                            try {

                                                status = 0;

                                                updateStatement = " UPDATE "
                                                        + " user_level_menu "
                                                        + " SET "
                                                        + " USER_LEVEL_MENU_STATUS = '" + menuStatus + "', "
                                                        + " UPDATE_BY = '" + groupName + "', "
                                                        + " UPDATE_DATE = CURRENT_TIMESTAMP "
                                                        + " WHERE "
                                                        + " GROUP_ID = '" + userGroupId + "' "
                                                        + " AND "
                                                        + " MENU_ID = '" + menuID + "' ";

                                                ps = connection.prepareStatement(updateStatement);
                                                status = ps.executeUpdate();

                                                if (status == 0) {

                                                    insertStatement = " INSERT INTO "
                                                            + " user_level_menu( "
                                                            + " MENU_ID, "
                                                            + " GROUP_ID, "
                                                            + " USER_LEVEL_MENU_STATUS, "
                                                            + " INSERT_BY, "
                                                            + " INSERT_DATE "
                                                            + " )VALUES( "
                                                            + "'" + menuID + "', "
                                                            + "'" + userGroupId + "', "
                                                            + "'" + menuStatus + "', "
                                                            + "'" + groupName + "', "
                                                            + " CURRENT_TIMESTAMP)";

                                                    ps = connection.prepareStatement(insertStatement);
                                                    status = ps.executeUpdate();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (status > 0) {
            setMessageColor("success");
            setMessageString("Menu permission saved successfully.");
        } else {
            setMessageColor("danger");
            setMessageString("Menu permission save failed!");
        }

        return SUCCESS;
    }

    public String addNewBook() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

//        if (bookDao == null) {
//            bookDao = new BookDao();
//        }
        if (connection != null) {

//            if (checkBookProperty(bookReference) == 0) {
//
//                if (bookIDGenerator() != null) {
//                    bookId = bookIDGenerator();
//                } else {
//                    bookId = "10001";
//                }
//
//                int checked = bookDao.newBook(connection, bookId, bookReference, bookName, bookTitle, bookQuantity,
//                        bookCategory, bookLocation, bookDescription, userId);
//
//                if (checked > 0) {
//                    setMessageColor("success");
//                    setMessageString("Book uploaded successfully");
//                } else {
//                    setMessageColor("danger");
//                    setMessageString("Book upload failed");
//                }
//            } else {
//                setMessageColor("danger");
//                setMessageString("Book Reference already exist, Please use different Book Reference");
//            }
        }

        return SUCCESS;
    }

    public String goSettingsPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserGroupInfoList(allList.getUserGroupInfo(connection));
            setMenuMasterInfoList(allList.getMenuParentInfo(connection));
        }

        setMenuParent("Admin");
        setMenuChlid("Settings");

        return SUCCESS;
    }

    /**
     * @return the userGroupId
     */
    public String getUserGroupId() {
        return userGroupId;
    }

    /**
     * @param userGroupId the userGroupId to set
     */
    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    /**
     * @return the userLevelMenuInfoList
     */
    public List<UserLevelMenu> getUserLevelMenuInfoList() {
        return userLevelMenuInfoList;
    }

    /**
     * @param userLevelMenuInfoList the userLevelMenuInfoList to set
     */
    public void setUserLevelMenuInfoList(List<UserLevelMenu> userLevelMenuInfoList) {
        this.userLevelMenuInfoList = userLevelMenuInfoList;
    }

    /**
     * @return the menuMasterInfoList
     */
    public List<MenuMaster> getMenuMasterInfoList() {
        return menuMasterInfoList;
    }

    /**
     * @param menuMasterInfoList the menuMasterInfoList to set
     */
    public void setMenuMasterInfoList(List<MenuMaster> menuMasterInfoList) {
        this.menuMasterInfoList = menuMasterInfoList;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the userGroupInfoList
     */
    public List<UserGroup> getUserGroupInfoList() {
        return userGroupInfoList;
    }

    /**
     * @param userGroupInfoList the userGroupInfoList to set
     */
    public void setUserGroupInfoList(List<UserGroup> userGroupInfoList) {
        this.userGroupInfoList = userGroupInfoList;
    }

    /**
     * @return the formMenuIdStatus
     */
    public String getFormMenuIdStatus() {
        return formMenuIdStatus;
    }

    /**
     * @param formMenuIdStatus the formMenuIdStatus to set
     */
    public void setFormMenuIdStatus(String formMenuIdStatus) {
        this.formMenuIdStatus = formMenuIdStatus;
    }

    /**
     * @return the menuChlid
     */
    public String getMenuChlid() {
        return menuChlid;
    }

    /**
     * @param menuChlid the menuChlid to set
     */
    public void setMenuChlid(String menuChlid) {
        this.menuChlid = menuChlid;
    }

    /**
     * @return the menuParent
     */
    public String getMenuParent() {
        return menuParent;
    }

    /**
     * @param menuParent the menuParent to set
     */
    public void setMenuParent(String menuParent) {
        this.menuParent = menuParent;
    }
}
