package com.action;

import com.common.AllList;
import com.common.DBConnection;
import com.model.BookDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.BookCategoryInfo;
import com.persistance.BookInfo;
import com.persistance.UserInfo;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;

public class BookAction extends ActionSupport {

    private String bookId;
    private String bookReference;
    private String bookName;
    private String bookTitle;
    private Integer bookQuantity;
    private String bookCategory;
    private String bookLocation;
    private String bookDescription;
    private String rentDate;
    private String returnDate;
    private String userid;
    private String bookCategoryName;
    private String categoryDescription;
    private String menuParent;
    private String menuChlid;
    private String messageString;
    private String messageColor;
    private Character status;
    private Integer bookCategoryId;

    private File userImage;
    private String userImageFileName;
    private String destPath;

    private Connection connection = null;
    private AllList allList = null;
    private BookDao bookDao = null;

    private List<BookCategoryInfo> bookCategoryInfos = null;
    private List<BookInfo> bookInfoList = null;
    private List<UserInfo> userInfoList = null;

    public String newBookRent() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (bookDao == null) {
            bookDao = new BookDao();
        }

        if (connection != null) {

            if (checkBookStock(bookId) > 1) {

                int checked = bookDao.newBookRent(connection, userid, bookId, bookQuantity, rentDate, returnDate, userId);

                if (checked > 0) {
                    setMessageColor("success");
                    setMessageString("Book rent successfully");
                } else {
                    setMessageColor("danger");
                    setMessageString("Book rent failed");
                }
            } else {
                setMessageColor("danger");
                setMessageString("Current stock of this Book ended");
            }
        }

        return SUCCESS;
    }

    public String addNewBook() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (bookDao == null) {
            bookDao = new BookDao();
        }

        if (connection != null) {

            if (checkBookProperty(bookReference) == 0) {

                if (bookIDGenerator() != null) {
                    bookId = bookIDGenerator();
                } else {
                    bookId = "10001";
                }

                try {

                    String ext = FilenameUtils.getExtension(userImageFileName);
                    String imgName = bookId + "." + ext;

                    destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/UPLOADED_IMAGE");

                    File fileToCreate = new File(destPath, imgName);
                    FileUtils.copyFile(userImage, fileToCreate);

                    int checked = bookDao.newBook(connection, bookId, bookReference, bookName, bookTitle, bookQuantity,
                            bookCategory, bookLocation, bookDescription, imgName, userId);

                    if (checked > 0) {
                        setMessageColor("success");
                        setMessageString("Book uploaded successfully");
                    } else {
                        setMessageColor("danger");
                        setMessageString("Book upload failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }
            } else {
                setMessageColor("danger");
                setMessageString("Book Reference already exist, Please use different Book Reference");
            }
        }

        return SUCCESS;
    }

    public String updateBook() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (bookDao == null) {
            bookDao = new BookDao();
        }

        if (connection != null) {

            if (checkBookProperty(bookReference) == 0) {

                System.out.println("bookId " + bookId);
                System.out.println("status " + getStatus());

                try {

//                    String ext = FilenameUtils.getExtension(userImageFileName);
//                    String imgName = bookId + "." + ext;
//
//                    destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/UPLOADED_IMAGE");
//
//                    File fileToCreate = new File(destPath, imgName);
//                    FileUtils.copyFile(userImage, fileToCreate);
                    int checked = 0;

//                            bookDao.newBook(connection, bookId, bookReference, bookName, bookTitle, bookQuantity,
//                            bookCategory, bookLocation, bookDescription, imgName, userId);
                    if (checked > 0) {
                        setMessageColor("success");
                        setMessageString("Book updated successfully");
                    } else {
                        setMessageColor("danger");
                        setMessageString("Book update failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }
            } else {
                setMessageColor("danger");
                setMessageString("Book Reference already exist, Please use different Book Reference");
            }
        }

        return SUCCESS;
    }

    public String addNewBookCategory() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (bookDao == null) {
            bookDao = new BookDao();
        }

        if (connection != null) {

            if (categoryNameCheck(bookCategoryName) == 0) {

                int checked = bookDao.newBookCategory(connection, bookCategoryName, categoryDescription, userId);

                if (checked > 0) {
                    setMessageColor("success");
                    setMessageString("Book Category created successfully");
                } else {
                    setMessageColor("danger");
                    setMessageString("Book Category create failed");
                }
            } else {
                setMessageColor("danger");
                setMessageString("Book Category Name already exist, Please use different Book Category Name");
            }
        }

        return SUCCESS;
    }

    public String updateBookCategory() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (bookDao == null) {
            bookDao = new BookDao();
        }

        if (connection != null) {

            if (categoryNameCheck(bookCategoryName) == 0) {

                System.out.println("bookCategoryId " + bookCategoryId);
                System.out.println("status " + getStatus());

                try {
                    int checked = 0;

//                            bookDao.newBook(connection, bookId, bookReference, bookName, bookTitle, bookQuantity,
//                            bookCategory, bookLocation, bookDescription, imgName, userId);
                    if (checked > 0) {
                        setMessageColor("success");
                        setMessageString("Book Category updated successfully");
                    } else {
                        setMessageColor("danger");
                        setMessageString("Book Category update failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }
            } else {
                setMessageColor("danger");
                setMessageString("Book Category Name already exist, Please use different Book Category Name");
            }
        }

        return SUCCESS;
    }

    private String bookIDGenerator() {

        String applicantID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.B_ID)+1),5,'0') "
                        + " AS BOOK_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(BOOK_ID, 1,5),UNSIGNED INTEGER) "
                        + " AS B_ID FROM book_info)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    applicantID = rs.getString("BOOK_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return applicantID;
    }

    private int checkBookStock(String bid) {

        int stock = 0;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " BI.BOOK_QUANTITY, "
                        + " SUM(UBM.BOOK_RENT_QUANTITY) AS RENT "
                        + " FROM "
                        + " book_info BI "
                        + " LEFT JOIN (user_book_map UBM) "
                        + " ON "
                        + " BI.BOOK_ID = UBM.BOOK_ID "
                        + " WHERE "
                        + " BI.BOOK_ID = '" + bid + "' "
                        + " GROUP BY "
                        + " BI.BOOK_ID ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    stock = (rs.getInt("BOOK_QUANTITY") - rs.getInt("RENT"));

                    System.out.println(rs.getInt("BOOK_QUANTITY"));
                    System.out.println(rs.getInt("RENT"));
                    System.out.println(stock);
                }
            } catch (Exception ex) {
                stock = 0;
                System.out.println(ex);
            }
        }

        return stock;
    }

    private int categoryNameCheck(String property) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT "
                        + " COUNT(BOOK_CATEGORY_NAME) "
                        + " TOTAL "
                        + " FROM "
                        + " book_category "
                        + " WHERE "
                        + " BOOK_CATEGORY_NAME = '" + property + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    private int checkBookProperty(String property) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT "
                        + " COUNT(BOOK_REF) "
                        + " TOTAL "
                        + " FROM "
                        + " book_info "
                        + " WHERE "
                        + " BOOK_REF = '" + property + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    public String goBookCategoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        setMenuParent("Book");
        setMenuChlid("BookCategory");

        return SUCCESS;
    }

    public String goCategoryHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookCategoryInfos(allList.getBookCategoryInfo(connection, "", userId, 0));
        }

        setMenuParent("Book");
        setMenuChlid("CategoryHistory");

        return SUCCESS;
    }

    public String goUploadBookPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookCategoryInfos(allList.getBookCategoryInfo(connection, "", userId, 0));
        }

        setMenuParent("Book");
        setMenuChlid("UploadBook");

        return SUCCESS;
    }

    public String goBookHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookInfoList(allList.getBookInfo(connection, "", userId, ""));
        }

        setMenuParent("Book");
        setMenuChlid("BookHistory");

        return SUCCESS;
    }

    public String allBookCategoriesByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookCategoryInfos(allList.getBookCategoryInfo(connection, "all", "", 0));
        }

        setMenuParent("Admin");
        setMenuChlid("AllBookCategories");

        return SUCCESS;
    }

    public String allBooksByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookInfoList(allList.getBookInfo(connection, "all", "", ""));
        }

        setMenuParent("Admin");
        setMenuChlid("AllBooks");

        return SUCCESS;
    }

    public String goBookRentPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getUserInfo(connection, "", "", "", "", userId));
            setBookInfoList(allList.getBookInfo(connection, "", userId, ""));
        }

        setMenuParent("Book");
        setMenuChlid("BookRent");

        return SUCCESS;
    }

    public String goBookRentHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getBookRentByUser(connection, "", userId, ""));
        }

        setMenuParent("Book");
        setMenuChlid("BookRentHistory");

        return SUCCESS;
    }

    public String allBookRentHistoryByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getBookRentByUser(connection, "all", "", ""));
        }

        setMenuParent("Admin");
        setMenuChlid("AllBookRentHistory");

        return SUCCESS;
    }

    public String viewBookDetails() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookCategoryInfos(allList.getBookCategoryInfo(connection, "", userId, 0));
            setBookInfoList(allList.getBookInfo(connection, "", "", bookId));
        }

        return SUCCESS;
    }

    public String viewBookCategoryDetails() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setBookCategoryInfos(allList.getBookCategoryInfo(connection, "", "", bookCategoryId));
        }

        return SUCCESS;
    }

    public String viewUserBookRentDetails() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");
        Integer groupId = (Integer) session.get("GROUPID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {
            setUserInfoList(allList.getBookRentByUser(connection, "", "", userid));
        }

        return SUCCESS;
    }

    public static void main(String[] args) {
        BookAction v = new BookAction();
        System.out.println(v.checkBookStock("10001"));
    }

    /**
     * @return the bookReference
     */
    public String getBookReference() {
        return bookReference;
    }

    /**
     * @param bookReference the bookReference to set
     */
    public void setBookReference(String bookReference) {
        this.bookReference = bookReference;
    }

    /**
     * @return the bookName
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * @param bookName the bookName to set
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    /**
     * @return the bookTitle
     */
    public String getBookTitle() {
        return bookTitle;
    }

    /**
     * @param bookTitle the bookTitle to set
     */
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    /**
     * @return the bookCategory
     */
    public String getBookCategory() {
        return bookCategory;
    }

    /**
     * @param bookCategory the bookCategory to set
     */
    public void setBookCategory(String bookCategory) {
        this.bookCategory = bookCategory;
    }

    /**
     * @return the bookDescription
     */
    public String getBookDescription() {
        return bookDescription;
    }

    /**
     * @param bookDescription the bookDescription to set
     */
    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the bookQuantity
     */
    public Integer getBookQuantity() {
        return bookQuantity;
    }

    /**
     * @param bookQuantity the bookQuantity to set
     */
    public void setBookQuantity(Integer bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    /**
     * @return the bookId
     */
    public String getBookId() {
        return bookId;
    }

    /**
     * @param bookId the bookId to set
     */
    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    /**
     * @return the bookInfoList
     */
    public List<BookInfo> getBookInfoList() {
        return bookInfoList;
    }

    /**
     * @param bookInfoList the bookInfoList to set
     */
    public void setBookInfoList(List<BookInfo> bookInfoList) {
        this.bookInfoList = bookInfoList;
    }

    /**
     * @return the bookLocation
     */
    public String getBookLocation() {
        return bookLocation;
    }

    /**
     * @param bookLocation the bookLocation to set
     */
    public void setBookLocation(String bookLocation) {
        this.bookLocation = bookLocation;
    }

    /**
     * @return the bookCategoryName
     */
    public String getBookCategoryName() {
        return bookCategoryName;
    }

    /**
     * @param bookCategoryName the bookCategoryName to set
     */
    public void setBookCategoryName(String bookCategoryName) {
        this.bookCategoryName = bookCategoryName;
    }

    /**
     * @return the categoryDescription
     */
    public String getCategoryDescription() {
        return categoryDescription;
    }

    /**
     * @param categoryDescription the categoryDescription to set
     */
    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    /**
     * @return the bookCategoryInfos
     */
    public List<BookCategoryInfo> getBookCategoryInfos() {
        return bookCategoryInfos;
    }

    /**
     * @param bookCategoryInfos the bookCategoryInfos to set
     */
    public void setBookCategoryInfos(List<BookCategoryInfo> bookCategoryInfos) {
        this.bookCategoryInfos = bookCategoryInfos;
    }

    /**
     * @return the menuParent
     */
    public String getMenuParent() {
        return menuParent;
    }

    /**
     * @param menuParent the menuParent to set
     */
    public void setMenuParent(String menuParent) {
        this.menuParent = menuParent;
    }

    /**
     * @return the menuChlid
     */
    public String getMenuChlid() {
        return menuChlid;
    }

    /**
     * @param menuChlid the menuChlid to set
     */
    public void setMenuChlid(String menuChlid) {
        this.menuChlid = menuChlid;
    }

    /**
     * @return the userInfoList
     */
    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    /**
     * @param userInfoList the userInfoList to set
     */
    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the rentDate
     */
    public String getRentDate() {
        return rentDate;
    }

    /**
     * @param rentDate the rentDate to set
     */
    public void setRentDate(String rentDate) {
        this.rentDate = rentDate;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the destPath
     */
    public String getDestPath() {
        return destPath;
    }

    /**
     * @param destPath the destPath to set
     */
    public void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    /**
     * @return the userImage
     */
    public File getUserImage() {
        return userImage;
    }

    /**
     * @param userImage the userImage to set
     */
    public void setUserImage(File userImage) {
        this.userImage = userImage;
    }

    /**
     * @return the userImageFileName
     */
    public String getUserImageFileName() {
        return userImageFileName;
    }

    /**
     * @param userImageFileName the userImageFileName to set
     */
    public void setUserImageFileName(String userImageFileName) {
        this.userImageFileName = userImageFileName;
    }

    /**
     * @return the status
     */
    public Character getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Character status) {
        this.status = status;
    }

    /**
     * @return the bookCategoryId
     */
    public Integer getBookCategoryId() {
        return bookCategoryId;
    }

    /**
     * @param bookCategoryId the bookCategoryId to set
     */
    public void setBookCategoryId(Integer bookCategoryId) {
        this.bookCategoryId = bookCategoryId;
    }
}
