package com.model;

import com.common.AllQueryStatement;
import com.persistance.BookCategoryInfo;
import com.persistance.BookInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class BookDao {

    public int newBook(
            Connection connection,
            String bookid,
            String reference,
            String name,
            String title,
            Integer quantity,
            String category,
            String bookLocation,
            String description,
            String img,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                BookInfo bookInfo = new BookInfo();

                bookInfo.setBookId(bookid);
                bookInfo.setBookRef(reference);
                bookInfo.setBookName(name);
                bookInfo.setBookTitle(title);
                bookInfo.setBookQuantity(quantity);
                bookInfo.setCategory(category);
                bookInfo.setBookLocation(bookLocation);
                bookInfo.setDescription(description);
                bookInfo.setImgUrl(img);
                bookInfo.setInsertBy(insertBy);

                List<BookInfo> infos = new ArrayList<BookInfo>();
                infos.add(bookInfo);

                ps = connection.prepareStatement(AllQueryStatement.insertBook(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int newBookCategory(
            Connection connection,
            String bookCatName,
            String description,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                BookCategoryInfo categoryInfo = new BookCategoryInfo();
                List<BookCategoryInfo> infos = new ArrayList<BookCategoryInfo>();

                categoryInfo.setBookCategoryName(bookCatName);
                categoryInfo.setCategoryDescription(description);
                categoryInfo.setInsertBy(insertBy);

                infos.add(categoryInfo);

                ps = connection.prepareStatement(AllQueryStatement.insertBookCategory(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int newBookRent(
            Connection connection,
            String userid,
            String bookid,
            Integer quantity,
            String rentDate,
            String returnDate,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                BookInfo bookInfo = new BookInfo();
                UserInfo userInfo = new UserInfo();

                bookInfo.setBookId(bookid);
                bookInfo.setBookQuantity(quantity);
                bookInfo.setRentDate(rentDate);
                bookInfo.setReturnDate(returnDate);
                bookInfo.setInsertBy(insertBy);

                userInfo.setUserid(userid);
                bookInfo.setUserInfo(userInfo);

                List<BookInfo> infos = new ArrayList<BookInfo>();
                infos.add(bookInfo);

                ps = connection.prepareStatement(AllQueryStatement.insertBookRent(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
