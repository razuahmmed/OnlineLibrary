package message;

import java.io.File;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmai {

    public static void main(String[] args) {

        String uname = "hasibalhasan1986";
        String upass = "bastopur.2013";
        String from = "hasibalhasan1986@gmail.com";
        String to = "razuahmmed2014@gmail.com";
        String to1[] = {"razuahmmed2014@gmail.com", "hasibalhasan1986@gmail.com", "razuahmmedseo@gmail.com", "mdsujauddinsheik@gmail.com"};

//        helpherJournalDevMethods(upass, from, to);
//
//        sendMailBySSL(uname, upass, from, to, "Test Gmail Message", "Hi everyone whatsup.... ?");
//
//        multipleEmailSender(from, upass, to1);
    }

    /**
     * *************************************** done methods start
     */
    public static void helpherJournalDevMethods(String password, String fromEmail, String toEmail) {

        //****************//
//        String filename = "";
//        String workingDir = System.getProperty("user.dir");
//        String absoluteFilePath = "";
//        String your_os = System.getProperty("os.name").toLowerCase();
//
//        if (your_os.indexOf("win") >= 0) {
//            //if windows
//            absoluteFilePath = workingDir + "\\" + filename;
//        } else if (your_os.indexOf("nix") >= 0 || your_os.indexOf("nux") >= 0 || your_os.indexOf("mac") >= 0) {
//            //if unix or mac
//            absoluteFilePath = workingDir + "/" + filename;
//        } else {
//            //unknow os?
//            absoluteFilePath = workingDir + "/" + filename;
//        }
//
//        System.out.println("your_os " + your_os);
//
//        try {
//
//            File file = new File(absoluteFilePath);
//            if (file.createNewFile()) {
//                System.out.println("Done");
//            } else {
//                System.out.println("File already exists!");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //****************//

//        link ....................http://www.journaldev.com/2532/javamail-example-send-mail-in-java-smtp
//        
        // without authentication   not working     
//        Properties props = System.getProperties();
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        Session session = Session.getInstance(props, null);
//        sendEmail(session, toEmail, "SimpleEmail Testing Subject", "SimpleEmail Testing Body");
//        
        // TLS Authentication done well
//        Properties props = new Properties();
//        props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
//        props.put("mail.smtp.port", "587"); //TLS Port
//        props.put("mail.smtp.auth", "true"); //enable authentication
//        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
//
//        //create Authenticator object to pass in Session.getInstance argument
//        Authenticator auth = new Authenticator() {
//            //override the getPasswordAuthentication method
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(fromEmail, password);
//            }
//        };
//        Session session = Session.getInstance(props, auth);
//        sendEmail(session, toEmail, "TLSEmail Testing Subject", "TLSEmail Testing Body");
//        
        // SSL Authentication  done well
        Properties props1 = new Properties();
        props1.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        props1.put("mail.smtp.socketFactory.port", "465"); //SSL Port
        props1.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        props1.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
        props1.put("mail.smtp.port", "465"); //SMTP Port

        Authenticator auth1 = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
//
        Session session1 = Session.getDefaultInstance(props1, auth1);
//        System.out.println("Session created");
//        sendEmail(session1, toEmail, "SSLEmail Testing Subject", "SSLEmail Testing Body");

        // with attachment  done well
//        sendAttachmentEmail(session1, fromEmail, toEmail, "SSLEmail Testing Subject with Attachment", "SSLEmail Testing Body with Attachment");
//        
        //with image   done well
        sendImageEmail(session1, fromEmail, toEmail, "SSLEmail Testing Subject with Image", "SSLEmail Testing Body with Image");
    }

    public static void sendEmail(Session session, String toEmail, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("no_reply@journaldev.com", "NoReply-JD"));

            msg.setReplyTo(InternetAddress.parse("no_reply@journaldev.com", false));

            msg.setSubject(subject, "UTF-8");

            msg.setText(body, "UTF-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            System.out.println("Message is ready");
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendAttachmentEmail(Session session, String fromEmail, String toEmail, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("ra@gmail.com", "NoReply-RA"));

            msg.setReplyTo(InternetAddress.parse("ra@gmail.com", false));

            msg.setSubject(subject, "UTF-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));

            // Create the message body part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText(body);

            // Create a multipart message for attachment
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Second part is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "D:/abc.pdf";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            msg.setContent(multipart);

            // Send message
            Transport.send(msg);
            System.out.println("EMail Sent Successfully with attachment!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendImageEmail(Session session, String fromEmail, String toEmail, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("ra@gmail.com", "NoReply-RA"));

            msg.setReplyTo(InternetAddress.parse("ra@gmail.com", false));

            msg.setSubject(subject, "UTF-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));

            // Create the message body part
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText(body);

            // Create a multipart message for attachment
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Second part is image attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "D:/image.png";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            //Trick is to add the content-id header here
            messageBodyPart.setHeader("Content-ID", "image_id");
            multipart.addBodyPart(messageBodyPart);

            //third part for displaying image in the email body
            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("<h1>Attached Image</h1>"
                    + "<img src='cid:image_id'>", "text/html");
            multipart.addBodyPart(messageBodyPart);

            //Set the multipart message to the email message
            msg.setContent(multipart);

            // Send message
            Transport.send(msg);
            System.out.println("EMail Sent Successfully with image!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMailBySSL(String username, String password, String fromEmail, String toEmail, String sub, String msg) {

        //Get properties object    
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.fallback", "false");

        //get Session   
        Session mailSession = Session.getDefaultInstance(props, null);
        mailSession.setDebug(true);

        //compose message    
        try {

            Message mailMessage = new MimeMessage(mailSession);
            mailMessage.setFrom(new InternetAddress(fromEmail));
            mailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            mailMessage.setSubject(sub);
            mailMessage.setText(msg);
//          mailMessage.setContent(msg, "text/html");
//            
            //send message  
            Transport transport = mailSession.getTransport("smtp");
            transport.connect("smtp.gmail.com", username, password);
            transport.sendMessage(mailMessage, mailMessage.getAllRecipients());

            System.out.println("message sent successfully");
        } catch (Exception e) {
            e.printStackTrace();
//            throw new RuntimeException(e);
        }
    }

    //  multiple email sender
    public static void multipleEmailSender(String from, String password, String to[]) {

        String host = "smtp.gmail.com";

        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", password);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props, null);

        try {

            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(from));

            InternetAddress[] toAddresses = new InternetAddress[to.length];

            for (int i = 0; i < to.length; i++) {
                toAddresses[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddresses.length; i++) {
                mimeMessage.addRecipient(Message.RecipientType.TO, toAddresses[i]);
            }

            mimeMessage.setSubject("Testing Subject");
            mimeMessage.setText("Dear Mail Crawler, spam to my email, please!");

            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, password);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();

            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *************************************** done methods end
     */
}
