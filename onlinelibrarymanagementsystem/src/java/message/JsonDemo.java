package message;

import com.google.gson.JsonObject;

public class JsonDemo {

    public static void main(String[] args) {
        JsonObject jo = new JsonObject();
        jo.addProperty("name", "foo");
        System.out.println(jo);
    }
}
